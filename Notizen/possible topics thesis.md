# Topics fo rthesis
- another subanalysis of infectiopedia
- mapping what guidelines are talking about and what they are not adressing
- public health perspective
- economic perspective
- machine-readable guideline perspective
- hospital implemntation perspective
- translation perspective
- what has an really really big impact?
- collaboration of various societies
- adaptation of guidelines for physicians
- infectious disease checklists
- knowledge-mangement for infectious disease physicians
- tools fast feedback loops for physicians
- personal tracking for physicians
- stack overflow for physicians / case discussion forums
- consent forms for discussing cases online
- patient education -> increase relevant infos regarding medicine for patients
- Telemedicine
- Open-sourcing something that is currently propriatry
- wikidata / wikipedia analysis of something...
- delphi on properties of infectious disease / pathogen / ... chapters


## what type
- systematic review
- network analysis
- qualitative interview
- survey quantitative
- intervention: controlled, randomised, ...
- analysis of collected data points -> more data science stuff

