# Research possible thesis topics

## Brainstorming descriptions for guideline coverage
- guideline coverage / scope / analysis / summary -izing / content
- clinical practice guideline coverage

### Search strings
Search review of international guideline coverage
Search scope of guidelines
Search analysis of scope of guidelines
Search analysis of guidelines
Search 'analysis of guidelines'
Search 'analysis of clinical practice guidelines'
Search analysis of clinical practice guidelines
Search review clinical practice guidelines analysis
Search clinical practice guidelines analysis
Search summarizing guidelines
Search summary guideline coverage
Search guideline coverage


## European audience questionnaire
- Where do we fail to implement evidence based knowledge in infectious diseases?
- How can we significantly facilitate bridging this knowledge to action gap in infectious diseases?
- How do you manage infetious disease knowledge within your institution?

## Mapping what humanity knows about infectious diseases
- How many studies in which field and for which disease
- What sort of studies within those fields
- How many guidelines about each disease / pathogen
- How many patients affected by the disease
- Do some sampling of these studies and ready the abstracts of several of them
- Go through all the infectious disease journals and add searches for all the keywords associated with infectious diseases form pubmed's MESH terms

### Search strings
Search (infectious diseases [Title]) AND chart[Title] Schema: all
Search (infectious diseases [Title]) AND chart[Title]
Search (infectious diseases [Title]) AND plotting[Title] Schema: all
Search (infectious diseases [Title]) AND plotting[Title]
Search (infectious diseases [Title]) AND mapping[Title]
Search (guidelines[Title]) AND analysis[Title]
Search (guidelines[Title]) AND coverage[Title]
Search (guidelines[Title]) AND mapping[Title]
Search mapping guidelines
Search mapping knowledge infectious diseases
