# Summary of most promising articles

## Title

### Main points

### Questions / Uncertainties from my side

### Comments


## Interventions to improve antibiotic prescribing practices for hospital inpatients
2017 Cochrane Review https://spiral.imperial.ac.uk/bitstream/10044/1/44428/2/Davey_et_al-2017-The_Cochrane_Library.pdf

### Main points
- This review includes 221 studies (58 RCTs, and 163 NRS). Most studies were from North America (96) or Europe (87). The remaining
  studies were from Asia (19), South America (8), Australia (8), and the East Asia (3). Although 62% of RCTs were at a high risk of bias,
  the results for the main review outcomes were similar when we restricted the analysis to studies at low risk of bias.
- More hospital inpatients were treated according to antibiotic prescribing policy with the intervention compared with no intervention
  based on 29 RCTs of predominantly enablement interventions (RD 15%, 95% confidence interval (CI) 14% to 16%; 23,394 partic-
  ipants; high-certainty evidence). This represents an increase from 43% to 58% .There were high levels of heterogeneity of effect size
  but the direction consistently favoured intervention.
- The duration of antibiotic treatment decreased by 1.95 days (95% CI 2.22 to 1.67; 14 RCTs; 3318 participants; high-certainty evidence)
  from 11.0 days. Information from non-randomised studies showed interventions to be associated with improvement in prescribing
  according to antibiotic policy in routine clinical practice, with 70% of interventions being hospital-wide compared with 31% for RCTs.
  The risk of death was similar between intervention and control groups (11% in both arms), indicating that antibiotic use can likely
  be reduced without adversely affecting mortality (RD 0%, 95% CI -1% to 0%; 28 RCTs; 15,827 participants; moderate-certainty
  evidence). Antibiotic stewardship interventions probably reduce length of stay by 1.12 days (95% CI 0.7 to 1.54 days; 15 RCTs; 3834
  participants; moderate-certainty evidence). One RCT and six NRS raised concerns that restrictive interventions may lead to delay in
  treatment and negative professional culture because of breakdown in communication and trust between infection specialists and clinical
  teams (low-certainty evidence).
- Both enablement and restriction were independently associated with increased compliance with antibiotic policies, and enablement
    enhanced the effect of restrictive interventions (high-certainty evidence). Enabling interventions that included feedback were probably
    more effective than those that did not (moderate-certainty evidence).
- We found high-certainty evidence that interventions are effective in increasing compliance with antibiotic policy and reducing duration
of antibiotic treatment.
- Lower use of antibiotics probably does not increase mortality and likely reduces length of stay.
- We included interventions relevant to improving antibiotic pre-
  scribing as outlined in the EPOC taxonomy (EPOC 2015).
  1. Audit and feedback defined as any summary of clinical
  performance of health care over a specified period of time.
  2. Education through meetings or distribution of educational
  materials.
  3. Educational outreach through academic detailing or review
  of individual patients with recommendation for change.
  4. Reminders provided verbally, on paper, in the workplace
  environment (e.g. posters or messages printed on equipment) or
  on computer.
  5. Structural: the influence on antibiotic prescribing of
  changing from paper to computerised records and of the
  introduction of new technology for rapid microbiology testing or
  measurement of inflammatory markers.
- In addition, we included the following restrictive interventions:
  selective reporting of laboratory susceptibilities; formulary restric-
  tion; requiring prior authorisation (expert approval) therapeutic
  substitution; and automatic stop orders.
- Restriction seems to have a larger effect then enablement  
- Enablement interventions + feedback is better than enablement interventions only
- We found no evidence of a difference in results for
  interventions that targeted antibiotic exposure (decision to treat or
  duration of all antibiotic treatment) versus the choice of antibiotic
  prescribed
- Interventions that included feedback were more effective than those
  that did not. However, there were too few studies with goal setting
  or action planning to assess their effect in addition to feedback.
- However, the re-
  sults suggest that restrictive interventions were less likely to have
  sustained effect if they did not include enablement
- We found evidence that removal of restriction, in Himmelberg
1991, Kallen 2009, Kim 2008, and Skrlin 2011, or of review and
recommend change (enablement, Standiford 2012) was associated
with reversal of intervention effect (Table 7). **This is an important issue because
                                                  the attractiveness of interventions will be reduced if improvement
                                                  resources cannot be moved on to new priorities. Restriction is a
                                                  relatively low-cost intervention, but it is worrying that an enabling
                                                  intervention (review and recommend change) apparently had no
                                                  sustained effect on clinical teams after being in place for seven
                                                  years (Standiford 2012).**
- There is an urgent need for co-ordinated, multi-
  centre research studies.

### Questions / Uncertainties from my side

### Comments
- Antimicrobial manage-
  ment teams might consider using evidence about effective feed-
  back from other clinical settings (Ivers 2012).



## Determinants of in-hospital antibiotic prescription behaviour: a systematic review and formation of a comprehensive framework
2019
https://www.sciencedirect.com/science/article/abs/pii/S1198743X18306281

### Main points
- Under such uncertainty and in a complex decisional context,   behavioural, social and cultural factors gain influence on decisions
  made with regard to antimicrobial therapy
- 9 studies in total, mostly qualitative interviews as studies, 2 quantitaive questionnaires
- This systematic review shows that fear of an adverse outcome
  because of untreated infection and (lack of) tolerance of uncertainty
  are prominent determinants of APB. Moreover, determinants con-
  cerning the subjective norm, such as reputation, were frequently
  identified as being important. It is remarkable that the negative
  consequences of antibiotic therapydboth at the individual patient
  level and at the population leveldwere only scarcely identified by
  physicians as a determinant of antimicrobial treatment decisions.
- **Really nice table included** 'Determinants of antibiotic prescription behaviour (APB) and examples of interventions'
    - e.g. for guidelines: Improving applicability of guidelines by
                           adapting them to:
                           - variants of the clinical syndrome
                           - specific patient populations
                           Letting prescribers participate in
                           hospital guideline formation to improve
                           support and adherence

### Questions / Uncertainties from my side



### Comments
- deep dive into the table recommended
- forward to Tobi


## Knowledge Management Implementation and the Tools Utilized in Healthcare for Evidence-Based Decision Making: A Systematic Review
2017
https://www.ncbi.nlm.nih.gov/pubmed/29217960


### Main points
- Some of the technologies that can
  be used for KM during knowledge transformation,
  i.e., the conversion of one form of knowledge to
  the other form of knowledge in the model include
  electronic meeting systems, Internet, group
  collaboration system to convert tacit knowledge
  into tacit knowledge.
- The
  difficulty is on the use of management techniques,
  the concepts to design and develop KM tools, the
  availability of multiple KM technologies, as well
  as their applications and usage.
- Some of the challenges reported in
  many of the studies reviewed in the present work
  are infrastructure (technological) constraints, lack
  of motivation of employees to share knowledge
  (12,15,33), system unreliability, lack of senior
  management support, organizational politics
  patients’ privacy issues (12,33), reluctance of
  clinicians to use ICT tools on daily basis-mainly
  due to lack of time (5)-, lack of attention to results
  and use of evidence, lack of incentives for
  documentation and dissemination, limited
  document and use of good practices, inadequate
  awareness about KM systems (16), expensive
  initial investment, poor quality of patient data or
  information (18,30,34), inequity in status among
  practitioners (i.e., inhibitor of knowledge sharing),
  organizational culture, missing a centralized
  knowledge-base system and lack of trust (19,36)
- Some
  of the reasons for the willingness of health
  professionals to share their knowledge include
  effective communication, managing personal
  knowledge, generating discussion about new
  concepts or ideas, finding answers to particular
  problems, staying informed about the latest news
  and activities of fellow colleagues, receiving
  desired help and feedback, increasing one’s social
  network, building a level of credibility (37),
  satisfaction in helping others and passion about
  some topics.
  
### Questions / Uncertainties from my side

### Comments
- Published in an ethiopian journal...
- pretty wordy article - should have been shorter



## Tools developed and disseminated by guideline producers to promote the uptake of their guidelines
2016, Cochrane Review
https://www.ncbi.nlm.nih.gov/pubmed/27546228

### Main points
- four cluster-RCTs that were conducted (in the management of non-specific low back pain and ordering thyroid-function tests.)
- The implementation tools evaluated targeted
  healthcare professionals; none targeted healthcare organisations or patients.
- One study used two short educational workshops tailored to barriers. In three studies the intervention consisted of the provision of
  paper-based educational materials, order forms or reminders, or both.
- A guideline tool developed by the
  producers of a guideline probably leads to increased adherence to the guidelines; median ARD (IQR) was 0.135 (0.115 and 0.159
  for the two studies respectively) at an average four-week follow-up (moderate certainty evidence), which indicates a median 13.5%
  greater adherence to guidelines in the intervention group. Providing healthcare professionals with a tool to improve implementation of
  a guideline may lead to little or no difference in costs to the health service.
- We could not draw any conclusions about
  our second objective, the comparative effectiveness of implementation tools, due to the small number of studies, the heterogeneity
  between interventions, and the clinical conditions that were targeted.
- Previous system-
  atic reviews have identified a range of interventions to support the
  implementation of guidelines (Grimshaw 2004)  
- include learning modules (which may be
  accredited with Continuing Medical Education (CME) points),
  education outreach visits (for example, academic detailing), com-
  munication tools (for example, press releases following the publi-
  cation of CPGs) or tailored formatting (for example, the wording
  of recommendations adapted for a target audience or local health
- Many guideline producers are work-
  ing on transforming their narrative CPGs into electronic format,
  as this may improve uptake through the implementation of CPGs
  in computer-based decision-support systems (Peleg 2010).
- The Guideline Implementability Ap-
  praisal (GLIA) instrument may be used by producers of guidelines
  to identify barriers to implementation during the design phase of
  a CPG and enable modifications prior to publication (Shiffman
  2005).
- For example, templates may be developed for users of CPGs
  to populate with local data in order to assess the applicability and
  impact of a CPG. The tailoring process is also important in en-
  gaging clinicians in the implementation process (Horbar 2004;
  Titler 2009).
- Local public health
  intelligence, expert advice and examples of best practice appear to
  be the most sought-after types of evidence, and in order for knowl-
  edge to be used it has to be translated into a practical resource
  (Gkeredakis 2011).
- Other determinants of the effective implementation of all CPGs
  are that they are clearly written, specific to a population and con-
  text, easy to use and that there is research evidence of its effective-
  ness for a particular end-user’s work context (Titler 2001).
- While
  CPGs are frequently written as text documents (Peleg 2010), stud-
  ies have shown that clinicians usually do not use written guide-
  lines during the actual care process (Wang 2002). Instead, patient-
  specific advice, particularly if delivered during patient encounters,
  is suggested to be more effective in changing clinician behaviour
  (Shea 1996). Thus, implementing CPGs in computer-based de-
  cision-support systems may improve the acceptance and appli-
  cation of guidelines in daily practice, particularly if the actions
  and observations of healthcare workers are monitored and advice
  is generated whenever a guideline is not followed (Wang 2002).
- Gagliardi 2011 identified eight features of CPGs that are desired by users of CPGs, or are associated with their use:
                                                                    1. Usability: the structure of the CPG has been modified to
                                                                    facilitate access, for example by providing a one-page summary
                                                                    of the recommendations;
                                                                    2. Adaptability: the CPG is available in different formats for
                                                                    different users or purposes, for example, print and electronic
                                                                    format, versions of the CPG are available for patients and
                                                                    caregivers;
                                                                    3. Validity: using a standardised system to grade the quality of
                                                                    evidence supporting each recommendation, for example
                                                                    GRADE;
                                                                    4. Applicability: the wording of the CPG recommendation
                                                                    has been tailored for different target audiences to support
                                                                    application of the guidance to local circumstances; this may
                                                                    include clinical and contextual information;
                                                                    5. Communicability: information to supplement the CPG,
                                                                    for example, educational resources for patients and information
                                                                    to support patient involvement;
                                                                    6. Accommodation: the addition of information on costs and
                                                                    resources, for example, the costing templates provided by NICE,
                                                                    and information on competencies and training required to
                                                                    implement the recommendations;
                                                                    7. Implementation: information on potential barriers and
                                                                    strategies for facilitating implementation, for example, a clinical
                                                                    assessment using a point-of-care template;
                                                                    8. Evaluation: performance measures or quality indicators for
                                                                    audit and monitoring.
- Types of interventions
    - Tailoring: • Tailoring of CPGs for different users to improve usability
             and applicability: examples include using different wording,
             varying the content, incorporating case studies of patients’
             experiences in the form of vignettes or narratives which
             contextualise the recommendations.
             • Different CPG formats adapted for different users/
             purposes, e.g. electronic (for use on a Personal Digital Assistant),
             paper, multimedia versions, summaries, the inclusion of
             algorithms.
    - Education: 
  • Learning modules (to include interactive learning modules)
  which may be accredited with Continuing Medical Education
  (CME) points, or to support the use of audit by junior doctors.
  • Instructions/templates, e.g. instructions, tools or templates
  to tailor guidelines/recommendations for local context (may also
  be used at the organisational level); point-of-care templates/
  forms (clinical assessment, standard orders).
  • Decision-support systems, e.g. electronic guidelines with
  built-in decision-support systems.
    - Tools targeting the patient
  • Producing versions of CPG recommendations for the
  public to improve provider-patient communication about
  guideline recommendations.
    - Tools targeting the organisation of care: 
  • Benchmarking tools, e.g. measures of gaps in performance
  to be used by those monitoring the implementation of CPGs
  (may also be used by individual healthcare professionals).
  • Costing templates as a budgetary aid (may also be used by
  individual healthcare professionals) to assess the resources
  required to implement the CPG.
  • Programme evaluation, audit tools, performance measures
  and quality indicators to evaluate the implementation of the
  CPG.  
    - Mass media interventions: 
      • Press releases following the publication of a CPG
- **We excluded the following types of studies/interventions:**
  1. Tools developed by groups of researchers, guideline groups
  on commission (no longer existing).
  2. Studies describing tools developed by guideline producers
  to improve guideline uptake without providing objective
  measurements of the effect of these interventions on professional
  practice or patient outcomes.
  3. Surveys of barriers/facilitators to the uptake of guidelines.
- Bekkering 2005 assessed the effectiveness of two (21⁄2 hours) ed-
  ucational training sessions for groups of 8 to 12 physiotherapists
  on adherence to CPGs for management of non-specific low back
  pain.
- Three studies evaluated the effectiveness of paper-based educa-
  tional materials or reminders, or both (Daucourt 2003; Fine 2003;
  Shah 2014).
- In Fine 2003 physicians received a multifaceted guideline inter-
  vention which included placement of a detail sheet in the patient’s
  medical record once a patient met guideline criteria for stability
  when receiving intravenous antibiotic therapy for pneumonia, a
  follow-up recommendation to the attending physician, and an of-
  fer to arrange follow-up home nursing care. The three site-specific
  detail sheets promoted any of three recommended action(s), i.e.
  conversion from intravenous to oral antibiotic therapy only, con-
  version and hospital discharge, or hospital discharge only.
- None of the interventions used in the included studies was theory-
  based. The implementation strategies used in the included studies were
         all supported by some evidence of their effectiveness and cited
         high-quality Cochrane Reviews, systematic reviews or overviews
         to justify their choice of strategies.
- In Bekkering 2005 the intervention was delivered face-to-face. In
  two studies (Daucourt 2003; Shah 2014) the paper-based inter-
  ventions were provided passively. In Fine 2003 one part of the
  intervention was delivered over the phone, and the rest passively
  in the form of paper-based materials.
- Two of the four included studies reported a measure of healthcare
  professional adherence to guidelines (Bekkering 2005; Daucourt
  2003) at four weeks; these were included in the calculations of the
  median absolute risk difference (ARD).
- Given that many CPG developers are providing tools to support
  implementation, they should consider embedding rigorous evalu-
  ations of the tools (e.g. randomised trials) to advance knowledge
  in this area. They should also aim to include economic analyses to
  determine the cost effectiveness of their tools
### Questions / Uncertainties from my side

### Comments
- worth looking at: Many guideline producers are work-
                    ing on transforming their narrative CPGs into electronic format,
                    as this may improve uptake through the implementation of CPGs
                    in computer-based decision-support systems (Peleg 2010).
- send to Janne and Tobi
- they only focused on the really important studies
- 4 studies werwe excluded because they were still running at the point of the review






## Information and Communication Technologies for the Dissemination of Clinical Practice Guidelines to Health Professionals: A Systematic Review
2016 
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5156823/

### Main points
- closing the knowledge-to-action gap
- digital CPGs can be continuously reviewed and updated with new
  evidence, while having the potential to be widely disseminated [6]. Furthermore, these Web-based tools
  provide both clinicians and consumers with a convenient method to access evidence-based CPGs [6].
- While the evidence of traditional KT strategies, such as printed
  educational materials [9], educational meetings [10], educational outreach [11], local opinion leaders [12],
  and audit and feedback [13], focusing on practice behavior change targeting health care professionals has
  been summarized [8], we have limited knowledge of the perceived usability and practice behavior among
  health professionals when using novel KT strategies such as ICTs for the dissemination of CPGs.
- Of the 21 studies that we included in our systematic review, 20 were randomized controlled trials (95%)
  and 1 was a controlled clinical trial (5%) [17-37] (Table 2). There were 7 primary ICT interventions that
  were used to disseminate CPGs: websites [17,22-25], computer software [26-28], Web-based workshops
  [20,29], computerized decision support systems (CDSSs) [30,31], electronic educational game [21], email
  [19,32], and multifaceted interventions that consisted of at least one ICT component [18,33-37].
- there were no apparent trends when comparing established and older ICTs (eg,
  email and computer software) versus newer emerging ICT interventions (eg, electronic educational games,
  Web-based workshops, and the multifaceted ICT interventions).
- Studies using websites to disseminate
  CPGs [17,22-25] demonstrated no improvements in knowledge [17,22,23,25], reduced barriers [25], or
  intentions to use CPGs [25]. There were positive effects for perceived usefulness [17] and perceived ease of
  use [22,23] (2 of 3 studies).
- Studies using computer software [26-28] demonstrated no improvements in
  knowledge [27] or skills [28], but an effect on perceived usefulness [26].
- We found that 2 studies using
  Web-based workshops [20,29] demonstrated improvements in knowledge [29] and perceived usefulness
  [29] and skills [20,29].
- Studies using CDSSs demonstrated variable results for skills, as 1 study [30]
  demonstrated a positive effect, while the other did not [31]. While both studies were compared with no
  intervention, it should be noted that in the latter study [31], the non-ICT intervention (empowered patient
  group) was the only group that demonstrated a positive effect when compared with no intervention.
- Studies
  using email [19,32] demonstrated improvements in knowledge [32] and skills [19,32]. 
- Studies using
  multifaceted ICT interventions [18,33-37] demonstrated improvements in knowledge [33,35] (2 of 3
  studies), perceived usefulness [34], perceived ease of use [33], intention to use CPGs [34], beliefs about
  capabilities [33], and skills [37] (1 of 2 studies). While the multifaceted interventions in this review mostly
  demonstrated positive findings for improvements in usability and practice behavior, it remains unclear
  whether they are in fact superior to single interventions. Grimshaw et al [8] revealed that effect sizes in
  multifaceted interventions do not necessarily increase with increasing number of components, and these
  types of interventions appear to be more costly than single interventions.

    
### Questions / Uncertainties from my side

### Comments
- maybe investigate further: a review by Squires et
  al [39] concluded that there is a lack of compelling evidence to demonstrate that multifaceted interventions
  are more effective than single interventions.









  

## Medical Wikis Dedicated to Clinical Practice: A Systematic Review
2015
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4392552/

### Main points
- Results: 
  Among 25 wikis included, 11 aimed at building an encyclopedia, five a textbook, three lessons, two
  oncology protocols, one a single article, and three at reporting clinical cases. Sixteen wikis were specialized
  with specific themes or disciplines. Fifteen wikis were using MediaWiki software as-is, three were hosted
  by online wiki farms, and seven were purpose-built. Except for one MediaWiki-based site, only purpose-
  built platforms managed detailed user disclosures. The owners were ten organizations, six individuals, four
  private companies, two universities, two scientific societies, and one unknown. Among 21 open
  communities, 10 required users’ credentials to give editing rights. The median information framework
  quality score was 6 out of 16 (range 0-15). Beyond this score, only one wiki had standardized peer-reviews.
  Physicians contributed to 22 wikis, medical learners to nine, and lay persons to four. Among 116 sampled
  articles, those from encyclopedic wikis had more videos, pictures, and external resources, whereas others
  had more posology details and better readability. The median creation year was 2007 (1997-2011), the
  median number of content pages was 620.5 (3-98,039), the median of revisions per article was 17.7
  (3.6-180.5) and 0.015 of talk pages per article (0-0.42). Five wikis were particularly active, whereas six
  were declining. Two wikis have been discontinued after the completion of the study.
- Professional medical wikis may be improved by using clinical cases, developing more detailed
  transparency and editorial policies, and involving postgraduate and continuing medical education learners
- In the study published in 2009 by Dobrogowska-Schlebusch [15], 52
    health-related wikis were included without specification of purpose or audience and assessed using the
    online Health Summit Working Group Information Quality tool (HSWG IQ tool) [19]. It globally showed
    poor quality scores, except for a few wikis having implemented expert moderation or peer reviews.
- **They provide a table for quality assessment of a medical wiki**
- We described characteristics related to
content (presence of pictures, videos, diagrams, posology details, evidence levels and external resources,
and numbers of words and references per article) and data related to edition (numbers of revisions and
authors per article, and related talks).
- The sampled articles were assessed with Flesch’s reading ease test
  adapted to each language and performed with automated hyphenation [34].
- Cross-reading our results, **the relevancy for clinicians of the medical wikis can be discussed according to
  four information properties: accuracy, readability, reliability, and currency.** Accuracy may be impaired in
  wikis not displaying a review policy (60%) and in those not delivering rules for organizing content (80%)
  [63,64]. The articles from encyclopedic wikis presented characteristics less relevant for professional use
  than the others, including more pictures, videos, and external resources but fewer posology details. The
  Flesch reading ease scores were globally low, especially for encyclopedic articles.
- In addition, articles were
  poorly referenced, and evidence level notifications were exceptional. Finally, 88% of the wikis had fewer
  than 50% of articles revised in the last year, and 24% of the sites were almost unused.
- The Medical
  Subject Headings (MeSH) indexing system is sometimes integrated, but it requires specific training for
  contributors, which is challenging in a multi-authoring context [76].
- Contrary to pure knowledge content, the
  frequent clinical case reports in medical wikis, supporting the emergence of concrete questions of practice,
  are likely to meet strong clinical interest.
- Among open communities, only 48% of medical
  wikis ask for credentials to register, with two requiring some proof [35,36].  As an alternative, users’
                                                                                 medical skills can be assessed during an automated registration including medical tests [58,78].
- In most wikis, weak and poorly collaborative activity jeopardizes content updates. The talk pages, when
  available, are exceptionally used, and the discussion threads included in forums or social networks are not
  directly connected to content pages [80].
- Apart from the severe reliability issues due to anonymity in Wikipedia [84], it has been shown that its development, based only on volunteering, leads articles to be unevenly readable, complete,
                                                                                           and reliable [17,20,85]. In our study, we paradoxically observed the highest page revision and discussion
                                                                                           levels in a small wiki reserved to a closed community [54]. This finding suggests that a strong user
                                                                                           commitment can overcome volunteering limitations.
- Therefore, as included in the wikiproject Medicine of Wikipedia [21], a policy specifying the nature
  and the limits of publicly accessible content is critical, and a model for displaying health information is
  needed [67,73].


### Questions / Uncertainties from my side

### Comments
- maybe investigate: The recent review of the literature about wikis and collaborative writing applications in health care by
                     Archambault et al broadly explored use patterns, quality of information, and knowledge translation
                     interests, and brought out a need for primary research on these applications [14]. http://www.jmir.org/2013/10/e210/
- **They provide a table for quality assessment of a medical wiki** -> we should use this as a boilerplate.
- **table with all the wikis they investigated available**
    - investigate these wikis
- investigate: wiki-based platforms used as non-
                 collaborative CMS, like Wikinu
- Recommend to read for Janne & Tobi      


## Wikis and Collaborative Writing Applications in Health Care: A Scoping Review
2013
https://www.jmir.org/2013/10/e210/                

### Main points
- Patients use wikis to share their
  experiences [43] and to find information [4]. The Canadian
  Agency for Drugs and Technologies in Health is exploring the
  use of wikis to update knowledge syntheses [44-46]; the United
  States’ National Institutes of Health is training its scientists in
  editing them [47,48]; and the World Health Organization is
  using a wiki format to update the International Classification
  of Diseases [49]. In addition, academic institutions have started using wikis to train health professionals [18,22,32,50-54]
- The most frequently used wiki
  software were MediaWiki (n=44), PBworks (n=8), Wikispaces
  (n=6), Wetpaint (n=6), Microsoft SharePoint (n=3), and Google
  Sites (n=3).  
- There were
  studies describing custom-built hybrid wikis (Wikibreathe (n=2)
  [100,101], Orthochina (n=1) [102], and FreyaWIKI (n=1) [103];
  the use of virtual learning environments (eg, Blackboard) to
  host wikis as aids for supporting educational activities (n=8);
  and the use of more sophisticated social media platforms (eg,
  Drupal [104], MijnZorgNet [105], Atlassian [76], and
  MinJournal [106]) that offer wikis and other social media such
  as blogs and social networking services.
- However, this
  study also concluded that all the wikis evaluated still needed
  improvements mainly concerning their completeness before
  they could safely be used for decision making.
- In total, 57 positive effects and 23 negative effects were
  identified. Among the categories of positive effects that we
  found, the most frequently reported were that CWAs improve
  collaboration (n=41), positively impact learning (n=30),
  influence psychological domains (n=28), facilitate knowledge
  management and accessibility to information (n=30), improve
  efficiency of health care (n=19), improve quality of health care
  (n=6), and prevent disease (n=3). The most frequently cited negative effects were that CWAs
                                    could have unfavorable impacts on knowledge management
                                    (n=14) such as information overload (n=4) and fast
                                    dissemination of poorly validated information (n=4), as well as
                                    on certain psychological domains (n=6) such as added stress
                                    (n=1) and negative emotions (n=5).
- **The five barriers most frequently mentioned, in order of
  frequency, were unfamiliarity with ICTs (n=8), time constraints
  and workload (n=6), lack of self-efficacy (belief in one’s
  competence to use ICT) (n=6), material resources—access to
  ICT (n=5), worries about the scientific quality of the information
  (n=5), and the presence of a closed wiki protected by a password
  (n=5). The five most recurrent facilitators were having had
  training (n=12), scientific quality of the information (n=10),
  ease of use (n=8), triability (n=7), presence of a community of
  practice or a community of learners (n=7), and presence of a
  moderator (n=7).**       
- Despite the controversy surrounding the use of information in
  Wikipedia in clinical decision making [57,65], a high proportion
  of health professionals and students are already using Wikipedia
  and other CWAs, with use apparently increasing, especially
  among younger professionals               
- Innovative developments such as
  semantic wikis [8,97,98,276,280] and bots [11,281] may
  decrease some of these negative effects. For example, to reduce
  the impression of information overload, certain authors are
  exploring semantic wikis to better organize and structure
  information based on a logical ontology [97,98]. Semantic wikis
  could help organize the knowledge being shared [8,276,280],
  potentially improve its meaningful use [282,283] and eventually
  allow its integration into intelligent Web-based decision-support
  tools [280]. Other authors are exploring the use of bots to
  decrease the risk of vandalism, biased editing, and spam
  [11,281].      
- One observational study demonstrated that involving
  moderators and experts in the sharing and curation of
  information within CWAs improves the quality of information
  [99]. However, as previous authors have demonstrated, finding
  ways to get these experts to participate remains a challenge
  [4,130,182,276,289].          
- **Authors from multiple fields
have explored modalities to stimulate participation**
[276,281,284,285,290-296]. Many facilitators reported from
fields other than health care include training [284,296], scientific
quality of the information resources [281,286,287], ease of use
[291], having access to integrated support tools [296], ease of
content editing [297-299], access to CWA [285], self-efficacy
[300,301], and the use of incentives [293,294,302-304].
- Some
  propose a set of scholarly metrics that would reward
  contributions to collaborative projects [130]. The journal RNA
  Biology stimulates contributions to Wikipedia by scholars by
  requiring that manuscripts be summarized for a Wikipedia page
  before accepting to publish the article [305]. The WikiGenes
  project has recognized the importance of authorship [10,36].
  Finally, similar to other fields [293,294,297,306], the presence
  of a community and the sense of community is a frequently
  reported facilitator that increases contributions by health care
  stakeholders.
### Questions / Uncertainties from my side

### Comments


## A Knowledge Management Framework and Approach for Clinical Development
2016
https://journals.sagepub.com/doi/10.1177/2168479016664773

### Main points
- The
  goal of managing knowledge is to improve organizational performance by getting the right information to the right people at
  the right time.
- Saving time: On a weekly basis, respondents indicated
  that individuals spent an average of 12.3% of their time
  searching for information and knowledge to perform the
  job and 9.3% of their time searching for subject matter
  expertise. This translates to a cost of $35,336/employee/
  year assuming a hypothetical full-time employee cost of
  $175,000. The time spent searching for knowledge
  decreased as employees became more experienced in
  clinical development.  
- Goals: 
    - know what knowledge exits, where to look for it and how to quickly find it
    - capture knowledge in a consistent and searchable manner
    - share lessons learned so new wokr always starts using current best practices
    - know who the experts are and how to contact them for advice
    - connect readily across boundaries of teams, functions and geographies
- steps
    - initiate: growing awareness
    - develop: localized and repeatable practices
    - Standardize: common processes and approaches
    - Opitmize: measured and adaptive
    - Innovate: Continuously improving practices
- Archival of out-of-date content.
- Identify the critical knowledge necessary to drive optimal outcomes tailored to organizational needs: Understand the knowledge needs
- **Define measures for use and effectiveness.**  
  
### Questions / Uncertainties from my side

### Comments
- pretty business related but still good
- Send to Tobi

## Crowdsourcing in health and medical research: a systematic review.
2020 https://www.ncbi.nlm.nih.gov/pubmed/31959234

### Main points
- Of the 17 studies that used crowdsourcing to evaluate
  surgical skills, 16 found the crowdsourcing evaluations
  were effective compared to expert evaluations. Crowdsour-
  cing evaluation typically involves videotaping a surgeon
  performing a skill in the surgical theatre and then upload-
  ing it onto a platform where an online crowd worker
  evaluates skill based on pre-specified criteria (Fig. 2).
- All 16
  studies paid non-expert, online, crowd workers small
  amounts of money to evaluate surgical skills. Sixteen stud-
  ies compared crowdsourcing approaches to conventional
  expert-panel approaches (see Additional file 2: Table S8,
  Additional file 3: Table S9, Additional file 6: Table S12).
  Low quality evidence from these studies suggested that
  crowd evaluation of surgical skill technique correlated with
  expert evaluation (see Additional file 3: Table S9). Moder-
  ate quality evidence suggested that crowdsourcing evalu-
  ation was faster than expert evaluation


### Questions / Uncertainties from my side

### Comments
- maybe relevant for future peer review process


## Optimizing design of research to evaluate antibiotic stewardship interventions: consensus recommendations of a multinational working group
2020 https://www.sciencedirect.com/science/article/pii/S1198743X1930477X

### Main points
- Despite the large and exponentially increasing number of
  studies published since the term antimicrobial stewardship was
  coined [5e7], evidence remains remarkably weak both for what
  specific antimicrobial use interventions are effective (in terms of
  mortality, length of stay, adverse events, resistance rates) and how
  antimicrobial use improvement strategies can be implemented to
  deliver the desired antimicrobial use in daily clinical practice [8].
- The evidence around improvement strategies is
  similarly weak, dominated by uncontrolled beforeeafter studies
  and inadequately performed interrupted time series analyses,
  mostly performed within single hospitals [10].
- Generally, the field of antimi-
  crobial stewardship research is dominated by single-centre
  observational and quasi-experimental studies which fail to deal
  optimally with risks of different forms of bias and that lack external
  validity [7,8].

### Questions / Uncertainties from my side

### Comments
- a ton of information of how to make a really good trial in the context of ABS
- send to Tobi - what we should not do :)
- send to Janne


## Information Architecture of Web-Based Interventions to Improve Health Outcomes: Systematic Review.
2018 https://www.ncbi.nlm.nih.gov/pubmed/29563076

### Main points
- IA is vital to website development. In commercial settings, good IA can enhance the ability of employees and customers to find information and decrease costs of Web redesign and maintenance [2]. However, IA is less often discussed in the context of digital spaces for behavior change and health outcomes. 
- Digital health interventions that mention IA primarily focus on navigation systems [3-5]. Generally, navigation systems concern the relationships among information or content at different levels—such as Web pages or sections. Structures can be hierarchical (top-down approach, with broader subjects encompassing smaller ones), matrix (movement along multiple dimensions), organic (free movement or exploration), or tunnel (sequential or linear organization) [6].
- Many experts in the field recommend and implement a tunnel (or tunnel hybrid) design for behavior change websites. A recent systematic review of Web-based health intervention studies showed that tunneling structures were used in 90% of interventions reviewed. Of the interventions reviewed, all of those with a mental health focus used tunnel designs [7]. Users of websites with a tunnel design navigate in a sequential fashion to optimize the ordering of information and maximize the effectiveness of the site, in much the same way that one would read a novel or watch a television series from start to finish [1].
- A tunnel experience is less likely to overwhelm users with information and options; it simplifies information consumption by defining what the user sees and when. In addition, tunnel design has the capacity to provide tailored “remedial” loops for users who do not pass certain knowledge test “check-points” or assessments [1]. In general, this type of feedback and reinforcement personalizes the experience and helps the individual progress through an intervention program. Evidence shows that personalized Web interventions are more efficacious in behavior change [1,9].
- A hybrid design that includes elements of tunnel design provides an opportunity to give users more structure and guidance while also allowing a user to break free from a “locked” information structure if they so choose [1].
- Conversely, free-form matrix—also known as organic—and hierarchical designs are less suitable for users unfamiliar with the content area (as is often the case for users of behavior change sites) because the freedom to explore information may make it difficult to navigate [1]. Additionally, these designs can make it more challenging for users to retrace their information search in order to review something previously seen [1].



### Questions / Uncertainties from my side

### Comments
- Send to Tobi


## What are the 'active ingredients' of interventions targeting the public's engagementwith antimicrobial resistance (AMR) and how might they work?
2018 https://researchonline.gcu.ac.uk/ws/files/26172772/McParland_et_al_2018_British_Journal_of_Health_Psychology.pdf


### Main points
- Critically, there have been no attempts to date to identify exactly what works, why it
  works, for whom, when, and in which circumstances. This lack of substantive detail
  makes it impossible to determine which intervention components are typically used or
  indeed which are associated with effectiveness.
- Behaviour change techniques (BCTs) are an effective way of coding the content of
  interventions, enabling the identification of the active ingredients within interventions.
  BCTs are described as the smallest component compatible with retaining the postulated
  active ingredients and can be used alone or in combination with other BCTs (Michie et al.,
  2013).
- On average,
  three or so identifiable mechanisms of action were identified focussing mainly upon the
  idea of changing behaviour through increasing knowledge levels, changing the local
  environment to enable the desired behaviour change, using social and professional role
  and identity and through using social influence to encourage behaviour change.
- We found that
  only four of these studies **reported an explicit theoretical foundation to behaviour change
  within their intervention**
- A greater range of TDF domains and BCTs were present within a few of the most
  successful interventions. These included promoting beliefs about capability and
  reinforcing behaviour, as well as encouraging a commitment to change behaviour
  and imagining future outcomes if behaviour is not changed, monitoring behaviour
  (with and without feedback) and providing information on the antecedents of
  behaviour. Although uncommon within the AMR interventions, these promising
  mechanisms may be important in future AMR intervention development.
- We identified clear
  common patterns in what has constituted the ‘active ingredients’ of many previous
  interventions. These have focussed upon increasing knowledge, changing the local
  environment, social/professional role and identity and social influence.
- There were also connections between the ‘Environmental context
  and resources’ mechanism of action and the ‘Prompts and cues’, ‘Restructuring the
  physical environment’, and ‘Adding objects to the environment’ mechanisms of change





### Questions / Uncertainties from my side

### Comments
- this is about the public not about physicians...


## Investigating the ways in which health informationtechnology can promote antimicrobial stewardship
2017 https://pure-oai.bham.ac.uk/ws/files/41863269/King_et_al_Investigating_the_ways_Journal_of_the_Royal_Society_of_Medicine_2017.pdf

### Main points
- HIT that supports AMS can come in many forms: electronic health records (EHRs) that
  record patient information such as diagnoses and previous medication sensitivities; ePrescribing
  systems, such as computerised physician order entry (CPOE) and/or clinical decision support (CDS)
  systems; 20 and monitoring systems that draw together information from disparate parts of the
  hospital, enabling, for example, the triaging of patients who require clinical intervention. These
  systems can make data accessible to support diagnosis, treatment decisions, and review of patients
  who may for example need the route of delivery of antibiotics to be changed (e.g. intravenous to
  oral) or those in whom treatment can be discontinued. 13
- At the point of care, nudges can come in the form of ‘accountable
  justification’ and ‘suggested alternatives’, as described by Meeker et al (2016).
- Over longer timeframes, HIT can also enable further audit and feedback, monitoring, and
  benchmarking of antimicrobial prescribing practice. These functions can support clinicians,
  pharmacists, and AMS committees in further promoting the AMS agenda as well as lead to inter-
  hospital comparison.
- Cloud-based EHRs are on the horizon which will raise the possibility of expanding HIT support from
  the antibiotic prescription and review lifecycle on the ward to an extended antibiotic lifecycle: one
  that follows patients through their hospital admission, discharge, and back into the community.


### Questions / Uncertainties from my side

### Comments
- Raises a really good point: **Where in the 'lifecycle' of antibiotic treatment do we want to intervene?**
    - Start smart
        - patient presentation
        - diagnosis
        - obtain cultures
        - empiric treatment
        - ...
    - Then focus
        - Reviewing the clinical diagnosis and assessing the need for continuing antibiotics
        - Review by AMS champions
        
## Physicians’ knowledge, perceptions, and behaviour towards antibiotic prescribing: A systematic review of the literature
2015 https://strathprints.strath.ac.uk/51981/1/Rabiatul_Salmi_etal_ERAIT_2015_Physicians_knowledge_perceptions_and_behaviour_towards_antibiotic.pdf

### Main points
- Physicians still have inadequate knowledge and misconceptions about
  antibiotic prescribing. Moreover, some physicians although aware that antibiotics are of limited benefit
  in some conditions still prescribed them. 
- Several factors influenced prescribing including patients’
  expectations, severity and duration of infections, uncertainty over diagnosis, potentially losing
  patients, and influence of pharmaceutical companies. 
- Pocket-sized guidelines seen as an important
  source of information for physicians.
- physicians in United Kingdom (61) and Peru (64) thought that prescribing narrow spectrum antibiotics,
  and prescribing them occasionally, could do little or no harm in terms of the development of antibiotic
  resistance.
- However they were concerned about
  patients’ dissatisfaction if antibiotics were not prescribed. Consequently, 64% of them thought it was
  necessary, and would eventually prescribe antibiotics to their patients with upper respiratory tract
  infections.
- Some doctors especially junior doctors or those in their internship
  always discussed URTI care with senior physicians (59%-98%), yet only 2.8% to 5.4% of them did not
  feel confident making decisions regarding treatment (29, 39, 65)
- In addition, in countries where payments for healthcare services are by the patient, i.e.
  out-of-pocket payments, physicians did not want to burden their patients with laboratory fees where
  they could not afford these
- Pulcini et al. showed that only 16% of young doctors in a French hospital knew
  the actual proportion of community acquired-Escherichia coli resistant to fluoroquinolones (65).
  Similarly, only 10% respondents in DR Congo knew about Klebsiella spp. resistant to ceftriaxone (39).
  However, physicians who specialized in infectious disease (ID) estimated antibiotic resistance rate
  more accurately compared to physicians in general medicine and other non-ID subspecialists (53).
- The majority of the physicians in the studies were familiar with guidelines related to antibiotics, but still
  some were not using them. For example in Sudan, only 32.6% of physicians referred to Sudan
  National Formulary (SNF)or the British National Formulary (BNF), while the remainder did not use any
  reference source for their decision making (57)
- the WHO Guidelines appeared to be the least popular as an information source as
  only 26.6% of them stated they used them as a reference (39). Physicians in Spain appreciated the
  recommendations contained in clinical guidelines; however, preferred these to be adapted to the local
  situation (59).
- Pocket-based antibiotic guidelines were rated as the most useful source of information. For example,
  the majority (98%) of the respondents in a US study and 50% in two public hospitals in Peru used
  ‘The Sanford Guide’, a pocket-sized guideline (39, 54).
- 20% of the physicians in US reported that their interaction with
pharmaceutical representatives influenced their own antimicrobial selections (54).









### Questions / Uncertainties from my side

### Comments

