# Goal

Searching for studies available to certain topics that have not been tackled by systematic reviews so far.

## Topics / Interventions

- Impact of Implementation / Development of local hospital guidelines
- Impact of local knowledge-management systems in hospitals
- Impact of shared treatment algorithms within hospitals compared to individuals deciding on treatment
- Impact of switching from (digital) Documents to Webservices
- Impact of knowledge platforms in hospitals (uptodate, other, ...) => Systematic Review of Computerized Knowledge Management Systems
- Determinants of success when crowdsourcing information
- Adverse events of the above
- Systematic Review of systematic Reviews

## Keywords for studies

### study
- controlled
- randomized / randomised
- trial
- pre post
- implementation

### topic
- knowledge management
- local guidelines
- Computerized Knowledge Management Systems
- UpToDate, Thomson Reuters, eMedicine, National Guideline Clearinghouse, AMBOSS
- wiki
- knowledge translation (KT)
- adaptation

("clinical trial"[Publication Type] OR "clinical trials as topic"[MeSH Terms] OR "clinical trial"[All Fields]) AND ("knowledge management"[MeSH Terms] OR ("knowledge"[All Fields] AND "management"[All Fields]) OR "knowledge management"[All Fields]) 
OR wiki[All Fields]


# Search queries systeamtic reviews

## Pubmed
- Search systematic review Computerized Knowledge Management Systems
- Search systematic review crowdsourcing
- Search Knowledge[Title] AND Management[Title] AND Implementation[Title] AND Tools[Title] AND Utilized[Title] AND Healthcare[Title] AND Evidence-Based[Title] AND Decision[Title] AND Making[Title] AND Systematic[Title] AND Review[Title]
- Search Knowledge Management Implementation and the Tools Utilized in Healthcare for Evidence-Based Decision Making: A Systematic Review.
- Search Medical[Title] AND wikis[Title] AND dedicated[Title] AND clinical[Title] AND practice[Title] AND systematic[Title] AND review[Title]
- Search Medical wikis dedicated to clinical practice: a systematic review.
- Search systematic review wikis
- Search systematic review knowledge platforms
- Search systematic review uptodate
- Search systematic review hospital knowledge management systems
- Search systematic review hospital knowledge management
- Search systematic review knowledge management
- Search local guideline implementation hospital
- Search guideline implementation hospital

and see CSV File in folder.

Next round:
- clinical practice guidelines infectious disease local adaptation -> nothing
- clinical practice guidelines local adaptation 
- systematic review guideline adaptation 
- local guideline adaptation 
- condensing guidelines 
- hospital guidelines [All Fields] 
- coverage guidelines [All fields] 



# Search queries studies

## Pubmed
- Similiar articles to Uptodate study  https://www.ncbi.nlm.nih.gov/pubmed?linkname=pubmed_pubmed&from_uid=22095750

## google scholar
- clinical trial wiki

# Takeaways
- 
- there is a systematic review on knowledge-management tools:  [Knowledge Management Implementation and the Tools Utilized in Healthcare for Evidence-Based Decision Making: A Systematic Review.][4]
- there is a chochrane review about:  [Tools developed and disseminated by guideline producers to promote the uptake of their guidelines.][3]
- [Medical wikis dedicated to clinical practice: a systematic review.][2]
- [Crowdsourcing in health and medical research: a systematic review.] [1]

[1]: https://www.ncbi.nlm.nih.gov/pubmed/31959234

[2]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4392552/

[3]: https://www.ncbi.nlm.nih.gov/pubmed/27546228

[4]: https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5615016/