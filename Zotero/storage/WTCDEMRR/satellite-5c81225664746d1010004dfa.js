_satellite.pushBlockingScript(function(event, target, $variables){
  dataLayer = window.dataLayer ? dataLayer : {};
dataLayer.event = dataLayer.event || [];

dataLayer.event.push = function (){
	var newLength =  Array.prototype.push.apply(this,arguments),
  		ev;
  
	if (arguments.length) {
		ev = arguments[0];

	    if (typeof ev === 'object') {
			if (typeof ev.eventData === 'object') {
				// send along entire eventData map
				_satellite.setVar('eventData',ev.eventData); 
			        window.console && console.log('Analytics eventData:');
			        window.console && console.log(ev.eventData);
        
			}
			if (typeof ev.eventName === 'string') {
				_satellite.track(ev.eventName);
			        window.console && console.log('Analytics eventName:');
			        window.console && console.log(ev.eventName);
			}
			dataLayer.event.length=0; 
		} else {
	       	  _satellite.notify('No Analytics Event object - dataLayer Event requires a root object with eventName and optional eventData')
	  	}
	}
  return newLength;
 }
 
});
