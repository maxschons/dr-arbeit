J Antimicrob Chemother 2013; 68: 2424 – 2427 doi:10.1093/jac/dkt360 Advance Access publication 12 September 2013

Downloaded from https://academic.oup.com/jac/article-abstract/68/11/2424/835084 by ZB MED / Med Abt. University Koeln user on 14 February 2020

Initiatives to improve appropriate antibiotic prescribing in primary care
Diane J. Harris*
NHS Southern Derbyshire Clinical Commissioning Group, Cardinal Square, 1st Floor East Point, 10 Nottingham Road, Derby DE1 3QT, UK
*Tel: +44-1332-868716; E-mail: diane.harris@southernderbyshireccg.nhs.uk
Inﬂuencing clinicians’ prescribing behaviour is important because inappropriate use and overuse of antibiotics are major drivers of antibiotic resistance. A systematic review of interventions for promoting prudent prescribing of antibiotics by general practitioners suggests that multifaceted interventions will maximize acceptability. This article reports how this type of approach has been used successfully in Derbyshire, UK over the last 4 years. The range of interventions that have been used includes educational meetings (both open group events and others targeted at higher prescribers in the surgery) using a supportive and guiding ethos; the provision of support materials aimed at empowering avoidance or delayed antibiotic prescribing, where appropriate, and improving patients’ knowledge and conﬁdence in self-management; and the production of different treatment guidelines incorporating key messages with evidence, indicating where antibiotics are unlikely to be of beneﬁt. Education on antibiotics in schools was a novel approach, which was developed in North Derbyshire to increase public awareness of the appropriate treatment for common illnesses without using antibiotics.
Keywords: GPs, education, guidelines, schools

Introduction

Initiatives to improve antibiotic prescribing

Antibiotic resistance and healthcare-associated infections pose a signiﬁcant threat to public health.1 This threat is magniﬁed by a declining number of new antimicrobial agents entering clinical practice. Broad-spectrum antibiotics (quinolones and cephalosporins) should be avoided when narrow-spectrum antibiotics remain effective, as they increase the risk of Clostridium difﬁcile infection (CDI), methicillin-resistant Staphylococcus aureus and resistant urinary tract infections (UTIs).2 The Chief Medical Ofﬁcer has highlighted the need for clinicians to preserve the effectiveness of antibiotics by giving clear evidence-based guidance on their appropriate use.3 Inappropriate prescribing increases resistance, wastes valuable resources, puts patients at risk of adverse effects and increases patient reattendance.4,5
It has been estimated that there are 120 million cases of respiratory tract infection (RTI) each year in the UK, with one-quarter of the population visiting their general practitioner (GP) with RTI symptoms, often expecting an antibiotic. However, RTIs are usually self-limiting, thus withholding antibiotic treatment rarely leads to serious complications. Despite this, 60% of all antibiotic prescriptions in primary care are for RTIs.6 A systematic review of research on interventions for promoting prudent prescribing for acute RTIs by GPs suggests that multifaceted interventions will maximize acceptability. Such interventions should allow GPs to reﬂect on their own prescribing, help reduce uncertainty about appropriate RTI management, educate GPs about appropriate prescribing, facilitate patient-centred care and be beneﬁcial to implement in practice.7 This article reports how this type of approach has been successfully used in the Derbyshire region of England over the last 4 years.

A range of interventions were introduced with the aims of reducing the prescribing of antibiotics, particularly quinolones and cephalosporins, and increasing public awareness of the appropriate treatment for common illnesses, particularly coughs and colds.
Education and support for GP practices
Between April 2009 and December 2012, GPs, non-medical prescribers, out-of-hours and other clinical staff across Derbyshire were invited to attend education sessions on healthcare-associated infections and evidence-based antibiotic prescribing. Sessions involved presentations from the local microbiologist, an antimicrobial pharmacist and an infection control nurse. GPs’ protected learning time was utilized to ensure high attendance. Graphs of antibiotic prescribing rates for all GP practices were circulated, followed by group discussions.
Various key resources were issued to all attendees, including the National Institute for Health and Care Excellence quick reference guide on antibiotic prescribing for RTIs,8 local treatment guidelines, other evidence-based summaries (e.g. National Prescribing Centre rapid reviews on the treatment of upper RTIs and simple UTIs9,10) and an article written by a GP practice on implementing change when managing infections in primary care.11
Subsequently, GP education and support visits were undertaken, either based on targeted prescribing performance or at the request of the practice. They were followed by further assistance, such as (i) conducting antibiotic audits, with the analysed results being fed back to the practice; (ii) providing support materials comprising posters and leaﬂets, including disease-speciﬁc (e.g.

# The Author 2013. Published by Oxford University Press on behalf of the British Society for Antimicrobial Chemotherapy. All rights reserved. For Permissions, please e-mail: journals.permissions@oup.com
2424

Downloaded from https://academic.oup.com/jac/article-abstract/68/11/2424/835084 by ZB MED / Med Abt. University Koeln user on 14 February 2020

Leading article

JAC

sore throat) leaﬂets produced by GPs at a Shropshire practice that were adaptable for other practices;11 and (iii) providing standard
operating procedures for delayed prescribing in dispensing and
non-dispensing practices.

assist with education sessions; and assist with press releases, as appropriate. One of these GPs has agreed to present at local education sessions to explain how their practice implemented change and reduced the prescribing of antibiotics.

Treatment guidelines
Evidence-based treatment guidelines were developed, based on Public Health England guidance, covering antimicrobial treatment; appropriate antibiotic prescribing and learning from local CDI cases; diagnosis and management of lower UTIs; and management of CDI.2 The link to the CDI guidance has been included on all positive laboratory reports for CDI so that GPs can access the guidance promptly.
Antibiotic Prescribing Lead GPs
Four GPs agreed to be Antibiotic Prescribing Leads (Champions), in order to have one lead for each of the four local [now Clinical Commissioning Group (CCG)] areas in Derbyshire. They help to promote the key antibiotic prescribing messages to practices; promote and

Other initiatives
Other initiatives aimed at improving prescribing in primary care and raising awareness of CDI are shown in Figure 1.
Antibiotic education in schools
Trained school health assistants delivered education on antibiotics across 180 primary schools in North Derbyshire as part of the hand hygiene lesson for children aged 7 –9 years. The children completed a quiz on antibiotics (Figure 2) and also took home items with key messages on the appropriate use of antibiotics, including a bookmark showing ‘Gerald the Giraffe’ who has a cold (available as Supplementary data at JAC Online), a certiﬁcate of attendance (available as Supplementary data at JAC Online) and a patient leaﬂet for their parents entitled ‘Get Well Soon

Messages included on urine sensitivity reports, reminding prescribers that co-amoxiclav, cephalosporins and ciproﬂoxacin may be associated with an increased risk of CDI. Local radio interviews by antimicrobial pharmacist for European Antibiotic Awareness Day. Paper on CDI and how community rates may be reduced circulated to GPs. Four Commissioning for Quality and Innovation indicators for prescribing developed for staff that provide primary healthcare out of hours. They included quality indicators for:
prescribing broad-spectrum antibiotics total antibiotic items proton pump inhibitors prescribing of anti-diarrhoeal medication (designed to check whether the patient may have CDI and arranging a test, as appropriate, instead of prescribing these medicines) Three monthly review of prescribing data for GP practices for: total antibiotic items broad-spectrum antibiotics proton pump inhibitors Reports and graphs of prescribing data sent to GP practices, so they could monitor their progress in comparison with their peers. Antibiotic prescribing data were collected, via Electronic Prescribing Analysis and Cost, items/STAR PU, as advised by the Antimicrobial Stewardship in Primary Care group.12 Education sessions on appropriate prescribing of antibiotics provided for dentists. Training sessions on the diagnosis and management of UTIs provided for district nurses. Education sessions on prescribing of antibiotics and healthcare-associated infections, including appropriate treatment of diarrhoea, provided for community pharmacists New antimicrobial treatment guidance is sent to community pharmacists to promote consistent advice to patients requesting treatment for common illnesses.
Figure 1. Other local initiatives implemented in Derbyshire aimed at improving antibiotic prescribing in primary care.

2425

Leading article

Downloaded from https://academic.oup.com/jac/article-abstract/68/11/2424/835084 by ZB MED / Med Abt. University Koeln user on 14 February 2020

Figure 2. Antibiotic quiz, with acknowledgements to Derbyshire Children and Young People’s Health Promotion Service.

Without Antibiotics’. Furthermore, every child entering school undergoing health screening checks (with parental consent) was asked to colour in the picture of Gerald the Giraffe, which they were given along with the patient leaﬂet for their parents.

Discussion
Following these initiatives, the prescribing of cephalosporins and quinolones decreased over 3 years (2009/10 to 2012/13), and is currently at least one-third less than the national average for

2426

Downloaded from https://academic.oup.com/jac/article-abstract/68/11/2424/835084 by ZB MED / Med Abt. University Koeln user on 14 February 2020

Leading article

JAC

cephalosporins and one-quarter less for quinolones. Feedback from clinicians who attended the education sessions was positive and included a variety of actions that they planned to undertake, including: ‘Use delayed prescriptions’; ‘Change prescribing for UTIs’; ‘Be more conﬁdent about not giving antibiotics’; and ‘Use leaﬂets’. Among the things the clinicians stated they had learnt were: ‘Evidence regarding using delayed prescriptions’; ‘Choice of antibiotic for UTIs in pregnancy’; ‘Urine sampling and testing’; and ‘Risk of CDI with different antibiotics’. This indicates that prescribers had enhanced their knowledge and found the sessions beneﬁcial.
The education on antibiotics in schools was well received. The younger children enjoyed colouring in Gerald the Giraffe, whilst the older children enjoyed doing the quiz and there were open discussions about antibiotics. Although public awareness was not measured, it is hoped that the outcome was increased awareness of appropriate treatment for common illnesses such as coughs and colds, as the children took home various materials with key messages for their parents, including a leaﬂet on antibiotics.
This initiative in schools is due to be started in southern Derbyshire in September 2013. Considerations for developing the initiative further include school nurses speaking to parents at parent evenings and giving them the leaﬂet on antibiotics, educating teachers about the topic so they can include it within ‘personal, social, health and economic’ lessons and using social media such as Twitter or Facebook to advertise the key messages to parents on getting well soon without using antibiotics.
In summary, this was a multifaceted approach aimed at improving the appropriateness of antibiotic prescribing. Although it was a local initiative the interventions described could readily be implemented or adapted for use in other areas.
Acknowledgements
The author is the Specialist Antimicrobial Pharmacist across all four Derbyshire CCGs: Southern Derbyshire CCG; North Derbyshire CCG; Erewash CCG; and Hardwick CCG (previously, Derbyshire County and Derby City Primary Care Trusts). Special appreciation goes to the staff at all four CCGs, including Steve Hulme, Kate Needham and all members of the medicines management teams; infection control nurses Sharon Lane, Sue Dakin and staff; and GP Prescribing and Antibiotic Leads. I also wish to thank consultant microbiologists and hospital pharmacists, including Dr Sarah Maxwell, Dr Fiona Donald, Dr Milind Khare, Dr Deborah Gnanarajah, Dr Farah Yazdani, Dr Julia Lacey and Caroline Dufﬁn for their advice and support. Further thanks go to Jayne Duly, Lead Professional, and the school health assistants at the School Community Nursing Team at Chesterﬁeld Royal Hospital NHS Foundation Trust. The interventions were developed in consultation with many others, including school nurses; the schools education adviser; medicines management pharmacists; the medical director; GPs and leads; infection control nurses; clinical leads for out-of-hours GPs; district nursing leads; non-medical prescribing leads; microbiologists; and hospital antimicrobial pharmacists.

Transparency declarations
None to declare.
Supplementary data
Supplementary data are available at JAC Online (http://jac.oxford journals.org/).
References
1 National Prescribing Centre. Key Therapeutic Topics—Medicines Management Options for Local Implementation. April 2012. http://www. npc.co.uk/qipp/key_therapeutics_topics.php (31 July 2013, date last accessed). 2 Public Health England (HPA). Management of Infection Guidance for Primary Care for Consultation and Local Adaptation. November 2012, revised February 2013. http://www.hpa.org.uk/Topics/InfectiousDiseases/ InfectionsAZ/PrimaryCareGuidance/ (31 July 2013, date last accessed). 3 Department of Health. Chief Medical Ofﬁcer Annual Report 2011: Volume 2. March 2013. http://www.gov.uk/government/publications/ chief-medical-ofﬁcer-annual-report-volume-2 (31 July 2013, date last accessed). 4 Costelloe C, Metcalfe C, Lovering A et al. Effect of antibiotic prescribing in primary care on antimicrobial resistance in individual patients: systematic review and meta-analysis. BMJ 2010; 340: c2096. 5 Little P, Gould C, Williamson I et al. Reattendance and complications in a randomised trial of prescribing strategies for sore throat: the medicalising effect of prescribing antibiotics. BMJ 1997; 315: 350– 2. 6 Williams M. Overuse of antibiotics for RTIs. Prescriber 2008; 19: 6 –9. 7 Tonkin-Crine S, Yardley L, Little P. Antibiotic prescribing for acute respiratory tract infections in primary care: a systematic review and meta-ethnography. J Antimicrob Chemother 2011; 66: 2215– 23. 8 National Institute for Health and Care Excellence. Quick Reference Guide 69. Respiratory Tract Infections—Antibiotic Prescribing. 2008. http:// guidance.nice.org.uk/CG69/QuickRefGuide/pdf/English (31 July 2013, date last accessed). 9 National Prescribing Centre. MeReC Rapid Review. Routine use of antibiotics to prevent serious complications of URTIs is not justiﬁed. November 2007. http://www.npc.nhs.uk/rapidreview/?p=23 (10 May 2013, date last accessed). 10 National Prescribing Centre. MeReC Rapid Review. Managing simple UTI: don’t routinely send urine for cultures, consider delayed antibiotics. March 2010. http://www.npc.nhs.uk/rapidreview/?p=1118 (10 May 2013, date last accessed). 11 Lowe C, Penney A. When managing infections in primary care, consideration must be given to address how to implement change. Pharm Pract. January/February 2009. 12 Ashiru-Oredope D, Sharland M, Charani E et al. Improving the quality of antibiotic prescribing in the NHS by developing a new Antimicrobial Stewardship Programme: Start Smart—Then Focus. J Antimicrob Chemother 2012; 67: Suppl 1: i51– 63.

2427

